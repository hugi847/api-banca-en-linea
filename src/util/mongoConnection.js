const https = require('https'),
  querystring = require('querystring'),
  Joi = require('joi'),
  path = require('path'),
  winston = require('winston'),
  logger = winston.createLogger({
    level: 'info',
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
      winston.format.label(''),
      winston.format.timestamp(),
      winston.format.printf(
        log => `[${log.timestamp}],[${path.relative('src', __filename)}],[${log.level}]: ${log.message}`
      )
    )
  });

module.exports = collection => {
  const mongoDB = 'bdbanca3mb79572',
    apiKey = 'RMBD14WGGZ5Pt50BLxAt_U2LvSyRgKWT',
    mlab = 'api.mlab.com',
    cURI = `/api/1/databases/${mongoDB}/collections/${collection}`;

  const getMongoHttpsOpts = resource => {
    return {
      hostname: mlab,
      port: 443,
      path: `${cURI}${resource || ''}?apiKey=${apiKey}`,
      agent: false,
      headers: {
        'Content-Type': 'application/json'
      }
    };
  };

  const validate = (data, joiSchema, res) => {
    let result = Joi.validate(data, joiSchema, (err, data) => {
      if (err) {
        logger.log('error', err);

        res
          .header('Content-Type', 'application/json')
          .status(400)
          .send({
            statusCode: 400,
            statusText: err.details
              .reduce((sal, act) => {
                sal.push(act.message);
                return sal;
              }, [])
              .join(',')
          })
          .end();
        return false;
      }
      return data;
    });

    return result ? Promise.resolve(result) : Promise.reject(result);
  };

  const send = (path, method, data, httpResponse, queryParams) => {
    const mongoHttpsOpts = getMongoHttpsOpts(path);
    return new Promise((resolve, reject) => {
      let reqBody = (method === 'POST' || method === 'PUT') && data ? JSON.stringify(data) : '';
      let params =
        method === 'GET' || method === 'DELETE'
          ? querystring.stringify(data || {})
          : queryParams
            ? querystring.stringify(queryParams)
            : '';
      logger.log('debug', `requestBody: ${reqBody}`);

      logger.log(
        'info',
        querystring.unescape(
          `${method} ${mongoHttpsOpts.path}${params ? '&' + params : ''}`
            .replace(/\?apiKey=[^&]+/, '')
            .replace(/&/, '?')
        )
      );

      https
        .request(
          Object.assign(mongoHttpsOpts, {
            path: `${mongoHttpsOpts.path}${params ? '&' + params : ''}`,
            method: method,
            'Content-Length': Buffer.byteLength(reqBody)
          }),
          response => {
            logger.log('info', `Response HTTP Code: ${response.statusCode}`);

            var bodyResponse = [];
            response.on('data', data => {
              bodyResponse.push(data);
            });

            response.on('end', () => {
              try {
                const fullBodyResponse = Buffer.concat(bodyResponse).toString();
                // console.log('debug', `responseBody: ${fullBodyResponse}`);
                logger.log('debug', `responseBody: ${fullBodyResponse}`);
                if (200 === response.statusCode) {
                  let responseObject = JSON.parse(fullBodyResponse);
                  if (
                    method === 'GET' &&
                    (!responseObject || (responseObject instanceof Array && !responseObject.length))
                  ) {
                    reject({
                      statusCode: 404,
                      statusText: 'Document not found'
                    });
                  } else {
                    resolve(responseObject);
                  }
                } else {
                  const errorMessage = JSON.parse(fullBodyResponse)
                    .message.replace(/^.+ and error message '([^']+)'/, '$1')
                    .replace('bdbanca3mb79572.', '');

                  return reject({
                    statusCode:
                      -1 < errorMessage.indexOf('duplicate key') ? 409 : errorMessage.indexOf('not found') ? 404 : 500,
                    statusText: errorMessage
                  });
                }
              } catch (err) {
                logger.log('error', err);
                reject({
                  statusCode: 500,
                  statusText: err.message || 'Internal server error'
                });
              }
            });
          }
        )
        .on('error', err => {
          logger.log('error', 'API response-error', err);
          reject({ statusCode: 500, statusText: 'Internal server error' });
        })
        .end(reqBody);
    })
      .then(response => {
        if (!httpResponse) {
          return Promise.resolve(response);
        }
        httpResponse
          .header('Content-Type', 'application/json')
          .send(JSON.stringify(response))
          .end();
      })
      .catch(err => {
        if (!httpResponse) {
          logger.log('error', 'SEND error' + JSON.stringify(err));
          return Promise.reject(err);
        }
        httpResponse
          .status(err.statusCode || 500)
          .header('Content-Type', 'application/json')
          .send(JSON.stringify({ statusCode: err.statusCode || 500, statusText: err.statusText || err.message || err }))
          .end();
        return Promise.reject(false);
      });
  };

  return {
    cURI,
    getMongoHttpsOpts,
    validate,
    send
  };
};
