const express = require('express'),
  port = process.env.port || 3000,
  bodyParser = require('body-parser'),
  app = express(),
  path = require('path'),
  winston = require('winston'),
  logger = winston.createLogger({
    level: 'info',
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
      winston.format.label(''),
      winston.format.timestamp(),
      winston.format.printf(
        log => `[${log.timestamp}],[${path.relative('src', __filename)}],[${log.level}]: ${log.message}`
      )
    )
  });

/**
 * Configuración para recuperar datos recibidos en formato application/json
 */
app.use(bodyParser.json());
/**
 * Configuración para recuperar datos recibidos en formato application/x-www-form-urlencoded
 */
app.use(bodyParser.urlencoded({ extended: true }));
/**
 * Configuración para permitir el acceso al API desde cualquier origen
 */
app.use((req, res, done) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  done();
});

/**
 * Mensaje de bienvenida (prueba del estado del servidor)
 */
app.get('/', (req, res) => res.send('¡Bienvenido a la Banca en Línea Tech University!'));

/**
 * Inclusión de las diferentes APIS disponibles en este servidor
 */
require('./api/customers/customers')(app);
require('./api/accounts/accounts')(app);
require('./api/users/users')(app);
require('./api/access/access')(app);
require('./api/controls/controls')(app);

/**
 * Inicio del servidor local HTTP
 */
app.listen(port, () => {
  logger.log('info', `Banca En Linea inicializada, escuchando en puerto ${port}`);
  logger.log('info', `http://127.0.0.1:${port}/`);
});
