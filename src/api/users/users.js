const apiPath = '/api/users',
  mongoConnection = require('../../util/mongoConnection')('users'),
  Joi = require('joi'),
  schemas = require('../schemas'),
  crypto = require('crypto');

module.exports = app => {
  /**
   * Crea una nueva cuenta de usuario en la colección users
   */
  app.post(`${apiPath}/`, (req, res) => {
    mongoConnection
      .validate(req.body, schemas.userSchema, res)
      .then(validUserData => {
        securePassword(validUserData);
        return mongoConnection.send('', 'POST', validUserData);
      })
      .then(user => {
        delete user.password;
        res
          .header('Content-Type', 'application/json')
          .status(200)
          .send(user)
          .end();
      });
  });

  /**
   * Actualiza la información básica de una cuenta de usuarios en la colección users
   */
  app.put(`${apiPath}/:_idUser`, (req, res) => {
    let validUserData;
    mongoConnection
      .validate(req.body, schemas.userUpdateSchema, res)
      .then(_validUserData => {
        validUserData = _validUserData;
        return mongoConnection.validate(
          req.params,
          Joi.object().keys({
            _idUser: schemas._idField.required().label('user_id')
          }),
          res
        );
      })
      .then(validUserID => {
        securePassword(validUserData);
        return mongoConnection.send(`/${validUserID._idUser}/`, 'PUT', { $set: validUserData });
      })
      .then(user => {
        delete user.password;
        res
          .header('Content-Type', 'application/json')
          .status(200)
          .send(user)
          .end();
      });
  });

  /**
   * Consulta la información de una cuenta de usuario en la colección users
   */
  app.get(`${apiPath}/access`, (req, res) => {
    mongoConnection
      .validate(
        req.query,
        Joi.object().keys({
          channel: Joi.string().required(),
          userID: schemas._userID.required()
        }),
        res
      )
      .then(validUserData => {
        return mongoConnection.send(
          '',
          'GET',
          {
            q: JSON.stringify(validUserData),
            fo: true
          },
          res
        );
      });
  });

  /**
   * Consulta la información de una cuenta de usuario en la colección users
   */
  app.get(`${apiPath}/:_id`, (req, res) => {
    mongoConnection
      .validate(
        req.params,
        Joi.object().keys({
          _id: schemas._idField.required().label('user_id')
        }),
        res
      )
      .then(validUserData => {
        return mongoConnection.send(`/${validUserData._id}`, 'GET', {}, res);
      });
  });

  /**
   * Consulta la información básica de una cuenta de usuario en la colección users
   */
  app.get(`${apiPath}`, (req, res) => {
    mongoConnection
      .validate(
        req.query,
        Joi.object().keys({
          channel: Joi.string().required(),
          userID: schemas._userID.required(),
          password: Joi.string()
        }),
        res
      )
      .then(validUserData => {
        securePassword(validUserData);
        return mongoConnection.send(
          '',
          'GET',
          {
            q: JSON.stringify(validUserData),
            f: JSON.stringify(validUserData.password ? { password: 0 } : { _id: 0, password: 0, customer: 0 }),
            fo: true
          },
          res
        );
      });
  });

  securePassword = user => {
    if (user.password) {
      user.password = crypto
        .createHash('MD5')
        .update(user.password)
        .digest('hex');
    }
  };
};
