const Joi = require('joi');

module.exports = (() => {
  const _hex = Joi.string().regex(/^([a-f0-9]{2})+$/, 'hexadecimal'),
    _largeStringField = Joi.string()
      .regex(/^[A-z0-9 ÁáÉéÍíÓóÚúÑñÜü]*$/, 'latin alphanumeric')
      .min(3)
      .max(50),
    _idField = _hex.min(24).max(24),
    _date = Joi.object().keys({
      $date: Joi.string().regex(
        /^20[0-9]{2}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])T(0[0-9]|1[0-9]|2[0-3]):(0[0-9]|[1-5][0-9]):(0[0-9]|[1-5][0-9])\.[0.9]{3}Z$/,
        'date'
      )
    }),
    _seccurePassword = Joi.string()
      .min(8)
      .regex(/([a-z][^a-z]*){3,}/, '3 lowercase letter minimum')
      .regex(/([A-Z][^A-Z]*){2,}/, '2 upercase letter minimum')
      .regex(/([0-9][^0-9]*){2,}/, '2 numbers minimum')
      .regex(/([\@\_\#\$\%\&\/\=\+\-\?\!])+/, '1 non alphanumeric (@_#$%&/=+-?!) symbol minimum'),
    _JSONFormat = Joi.string()
      .regex(/^\{.+\}$/, 'JSON Format')
      .min(3),
    _customerID = Joi.number()
      .min(1)
      .max(9999999999),
    _accountID = _customerID,
    _userID = Joi.string().email({ minDomainAtoms: 2 });

  const userSchema = Joi.object().keys({
    channel: Joi.string()
      .min(4)
      .max(4)
      .required(),
    userID: _userID.required(),
    password: _seccurePassword.required(),
    customer: _idField.required(),
    status: Joi.string()
      .valid(['ACTIVE', 'LOCKED', 'INACTIVE'])
      .required()
  });

  const userUpdateSchema = Joi.object()
    .keys({
      password: _seccurePassword,
      status: Joi.string().valid(['ACTIVE', 'LOCKED', 'INACTIVE'])
    })
    .min(1);

  const customerSchema = Joi.object().keys({
    customerID: _customerID.required(),
    name: _largeStringField.required(),
    lastName: _largeStringField.required(),
    address: Joi.object().keys({
      street: _largeStringField.required(),
      town: _largeStringField.required(),
      city: _largeStringField.required(),
      country: _largeStringField,
      postalCode: Joi.string()
        .alphanum()
        .min(5)
        .max(10)
    }),
    email: Joi.string()
      .email({ minDomainAtoms: 2 })
      .required(),
    accounts: Joi.array()
      .items(_idField)
      .min(1)
      .unique()
  });

  const accountSchema = Joi.object().keys({
    accountID: _accountID.required(),
    accountName: Joi.string()
      .min(3)
      .max(20),
    credit: Joi.boolean().required(),
    limit: Joi.number()
      .precision(2)
      .min(0)
      .max(999999999.99)
      .required(),
    balance: Joi.number()
      .precision(2)
      .min(0)
      .max(0)
  });

  const accountUpdateSchema = Joi.object()
    .keys({
      accountName: Joi.string()
        .min(3)
        .max(20),
      limit: Joi.number()
        .precision(2)
        .min(0)
        .max(999999999.99)
    })
    .min(1);

  const operationSchema = Joi.object().keys({
    type: Joi.string()
      .valid(['Transfer', 'Pay', 'Pay credit card'])
      .required(),
    concept: _largeStringField,
    amount: Joi.number()
      .precision(2)
      .min(0.01)
      .max(99999.99)
      .positive()
      .required(),
    referencedAccount: _accountID.required()
  });

  const accessSchema = Joi.object().keys({
    user: _idField.required(),
    customer: _idField.required(),
    channel: Joi.string()
      .min(4)
      .max(4)
      .required()
  });

  const accessControlSchema = Joi.object().keys({
    channel: Joi.string()
      .min(4)
      .max(4)
      .required(),
    user: _idField.required().optional(),
    uris: Joi.array()
      .items(Joi.string().min(4))
      .min(1)
      .unique(),
    methods: Joi.array()
      .items(Joi.string().valid(['POST', 'PUT', 'GET', 'DELETE']))
      .min(1)
      .unique()
  });

  const paginationSchema = Joi.alternatives().try([
    Joi.object().keys({
      fo: Joi.boolean().required(),
      q: _JSONFormat.required(),
      f: _JSONFormat,
      sk: Joi.number().min(1)
    }),
    Joi.object().keys({
      q: _JSONFormat,
      f: _JSONFormat,
      s: _JSONFormat,
      sk: Joi.number().min(1),
      l: Joi.number()
        .min(1)
        .max(200)
        .required()
    }),
    Joi.object().keys({
      c: Joi.boolean().required(),
      q: _JSONFormat,
      sk: Joi.number().min(1),
      l: Joi.number()
        .min(1)
        .max(200)
    })
  ]);

  return {
    _idField,
    _largeStringField,
    _seccurePassword,
    _userID,
    userSchema,
    userUpdateSchema,
    _customerID,
    customerSchema,
    _accountID,
    accountSchema,
    accountUpdateSchema,
    operationSchema,
    accessSchema,
    accessControlSchema,
    paginationSchema
  };
})();
