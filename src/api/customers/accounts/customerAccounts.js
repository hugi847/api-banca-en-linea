const customerAccountsApiPath = '/api/customers',
  customersMongoConnection = require('../../../util/mongoConnection')('customers'),
  accountsMongoConnection = require('../../../util/mongoConnection')('accounts'),
  Joi = require('joi'),
  schemas = require('../../schemas');

module.exports = app => {
  /**
   * Asocia una nueva cuenta al cliente en la colección customers
   */
  app.put(`${customerAccountsApiPath}/:_idCustomer/accounts`, (req, res) => {
    let validCustomerID, validAccountID;
    customersMongoConnection
      .validate(
        req.params,
        Joi.object().keys({
          _idCustomer: schemas._idField.required().label('customer_id')
        }),
        res
      )
      .then(_validCustomerID => {
        validCustomerID = _validCustomerID;
        return accountsMongoConnection.validate(
          req.body,
          Joi.object().keys({
            _idAccount: schemas._idField.required().label('account_id')
          }),
          res
        );
      })
      .then(_validAccountID => {
        validAccountID = _validAccountID;
        return customersMongoConnection.send(
          '',
          'GET',
          {
            q: JSON.stringify({
              _id: {
                $oid: validCustomerID._idCustomer
              }
            }),
            f: JSON.stringify({ _id: 0, accounts: 1 }),
            fo: true
          },
          res
        );
      })
      .then(customer => {
        if (customer.accounts.indexOf(validAccountID._idAccount) > -1) {
          res
            .header('Content-Type', 'application/json')
            .status(409)
            .send({
              statusCode: 400,
              statusText: 'Customer already have this account'
            })
            .end();
          return Promise.reject(false);
        }

        return customersMongoConnection.send(
          `/${validCustomerID._idCustomer}/`,
          'PUT',
          { $push: { accounts: validAccountID._idAccount } },
          res
        );
      });
  });

  /**
   * Consulta la información general de las cuentas del cliente en la colección accounts
   */
  app.get(`${customerAccountsApiPath}/accounts`, (req, res) => {
    customersMongoConnection
      .validate(
        { _idCustomer: req.headers.customer },
        Joi.object().keys({
          _idCustomer: schemas._idField.required().label('customer _id')
        }),
        res
      )
      .then(validCustomerID => {
        return customersMongoConnection.send(`/${validCustomerID._idCustomer}`, 'GET', null);
      })
      .then(customer => {
        if (!customer.accounts.length) {
          return Promise.reject({
            statusCode: 404,
            statusText: 'Customer doesn not have associated accounts'
          });
        }

        return accountsMongoConnection.send(
          ``,
          'GET',
          {
            q: JSON.stringify({
              _id: {
                $in: customer.accounts.map(item => {
                  return { $oid: item };
                })
              }
            })
          },
          res
        );
      })
      .catch(err => {
        if (false !== err) {
          console.log(err);

          res
            .header('Content-Type', 'application/json')
            .status(err.statusCode)
            .send(err)
            .end();
        }
      });
  });

  /**
   * Importa el módulo de operaciones sobre cuentas del cliente
   */
  require('./operations/customerAccountOperations')(app);
};
