const customerAccountsOperationsApiPath = '/api/customers/accounts/:_idAccount/operations',
  customersMongoConnection = require('../../../../util/mongoConnection')('customers'),
  accountsMongoConnection = require('../../../../util/mongoConnection')('accounts'),
  operationsMongoConnection = require('../../../../util/mongoConnection')('operations'),
  Joi = require('joi'),
  schemas = require('../../../schemas'),
  path = require('path'),
  winston = require('winston'),
  logger = winston.createLogger({
    level: 'info',
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
      winston.format.label(''),
      winston.format.timestamp(),
      winston.format.printf(
        log => `[${log.timestamp}],[${path.relative('src', __filename)}],[${log.level}]: ${log.message}`
      )
    )
  });

module.exports = app => {
  /**
   * Registra una operación entre la cuenta del cliente y una cuenta de referencia
   */
  app.post(`${customerAccountsOperationsApiPath}`, (req, res) => {
    let validOperationData,
      validCustomerID,
      customer,
      validAccountID,
      account,
      referencedAccount,
      operation,
      referencedOperation;
    operationsMongoConnection
      .validate(req.body, schemas.operationSchema, res)
      .then(_validOperationData => {
        validOperationData = _validOperationData;
        validOperationData.date = { $date: new Date().toISOString() };
        validOperationData.amount = -validOperationData.amount;
        return customersMongoConnection.validate(
          { _idCustomer: req.headers.customer },
          Joi.object().keys({
            _idCustomer: schemas._idField.required().label('customer _id')
          }),
          res
        );
      })
      .then(_validCustomerID => {
        validCustomerID = _validCustomerID;
        return accountsMongoConnection.validate(
          { _idAccount: req.params._idAccount },
          Joi.object().keys({
            _idAccount: schemas._idField.required().label('account _id')
          }),
          res
        );
      })
      .then(_validAccountID => {
        validAccountID = _validAccountID;
        return getCustomer(validCustomerID);
      })
      .then(_customer => {
        customer = _customer;
        if (customer.accounts.indexOf(validAccountID._idAccount) < 0) {
          return Promise.reject({
            statusCode: 400,
            statusText: 'Ivalid account_id'
          });
        }
        return getAccount(validAccountID);
      })
      .then(_account => {
        account = _account;
        if (account.balance < -validOperationData.amount) {
          return Promise.reject({
            statusCode: 400,
            statusText: 'Insufficient balance in this account to complete the operation'
          });
        }
        return getReferencedAccount({ accountID: validOperationData.referencedAccount });
      })
      .then(_referencedAccount => {
        referencedAccount = _referencedAccount;
        validOperationData.accountID = account._id.$oid;
        return updateAccountBalanceOperation(account, validOperationData);
      })
      .then(account => {
        return registerOperation(validOperationData);
      })
      .then(_operation => {
        operation = _operation;
        validOperationData.accountID = referencedAccount._id.$oid;
        validOperationData.referencedAccount = account.accountID;
        validOperationData.amount = -operation.amount;
        validOperationData.tipo =
          validOperationData.type === 'Traspaso'
            ? 'Traspaso'
            : validOperationData.type === 'Pago a cuenta de tercero'
              ? 'Abono'
              : validOperationData.type === 'Pago tarjeta de credito'
                ? 'Pago tarjeta de credito'
                : 'Otro';
        return updateAccountBalanceOperation(referencedAccount, validOperationData);
      })
      .then(account => {
        return registerOperation(validOperationData);
      })
      .then(operation => {
        res
          .header('Content-Type', 'application/json')
          .status(200)
          .send({
            statusCode: 200,
            statusText: 'Operation completed suceessfully'
          })
          .end();
      })
      .catch(err => {
        logger.log('error', `... End with error: ${JSON.stringify(err)}`);
        res
          .header('Content-Type', 'application/json')
          .status(err.statusCode)
          .send(err)
          .end();
      });
  });

  /**
   * Registra una operación entre la cuenta del cliente y una cuenta de referencia
   */
  app.get(`${customerAccountsOperationsApiPath}`, (req, res) => {
    let validOperationData, validCustomerID, customer, validAccountID;

    operationsMongoConnection
      .validate(req.query, schemas.paginationSchema, res)
      .then(_validOperationData => {
        validOperationData = _validOperationData;
        return customersMongoConnection.validate(
          { _idCustomer: req.headers.customer },
          Joi.object().keys({
            _idCustomer: schemas._idField.required().label('customer _id')
          }),
          res
        );
      })
      .then(_validCustomerID => {
        validCustomerID = _validCustomerID;
        return accountsMongoConnection.validate(
          { _idAccount: req.params._idAccount },
          Joi.object().keys({
            _idAccount: schemas._idField.required().label('account _id')
          }),
          res
        );
      })
      .then(_validAccountID => {
        validAccountID = _validAccountID;
        return getCustomer(validCustomerID);
      })
      .then(_customer => {
        customer = _customer;
        if (customer.accounts.indexOf(validAccountID._idAccount) < 0) {
          return Promise.reject({
            statusCode: 400,
            statusText: 'Ivalid account_id'
          });
        }

        for (key in validOperationData) {
          if (/^\s*[\{\[].+[\}\]]\s*$/.test(validOperationData[key])) {
            validOperationData[key] = JSON.parse(validOperationData[key]);
          }
        }

        if (!validOperationData.q) {
          validOperationData.q = {};
        }
        validOperationData.q.accountID = validAccountID._idAccount;

        for (key in validOperationData) {
          if (typeof validOperationData[key] === 'object' || typeof validOperationData[key] === 'array') {
            validOperationData[key] = JSON.stringify(validOperationData[key]);
          }
        }

        return operationsMongoConnection.send('', 'GET', validOperationData, res);
      })
      .catch(err => {
        if (err) {
          logger.log('error', `... End with error: ${err}`);
          res
            .header('Content-Type', 'application/json')
            .status(err.statusCode)
            .send(err)
            .end();
        }
      });
  });

  const getCustomer = customer => {
    logger.log('info', 'Getting customer...');
    return customersMongoConnection.send(`/${customer._idCustomer}`, 'GET');
  };

  const getAccount = account => {
    logger.log('info', 'Getting account info...');
    return accountsMongoConnection.send(`/${account._idAccount}`, 'GET');
  };

  const getReferencedAccount = account => {
    logger.log('info', 'Getting referenced account info...');
    return accountsMongoConnection.send('', 'GET', { q: JSON.stringify({ accountID: account.accountID }), fo: true });
  };

  const updateAccountBalanceOperation = (account, operation) => {
    logger.log('info', 'Updating account balance...');
    return accountsMongoConnection.send(
      '',
      'PUT',
      {
        $set: { balance: account.balance + operation.amount }
      },
      null,
      {
        q: JSON.stringify({
          _id: account._id,
          balance: account.balance
        }),
        fo: true
      }
    );
  };

  const registerOperation = operation => {
    logger.log('info', 'Registering operation...');
    return operationsMongoConnection.send('', 'POST', operation);
  };
};
