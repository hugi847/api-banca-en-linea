const apiPath = '/api/customers',
  mongoConnection = require('../../util/mongoConnection')('customers'),
  Joi = require('joi'),
  schemas = require('../schemas');

module.exports = app => {
  /**
   * Guarda un nuevo cliente en la colección customers
   */
  app.post(`${apiPath}/`, (req, res) => {
    mongoConnection.validate(req.body, schemas.customerSchema, res).then(validCustomerData => {
      mongoConnection.send('', 'POST', validCustomerData, res);
    });
  });

  /**
   * consulta la información de un cliente específico por "_id" en la colección customers
   */
  app.get(`${apiPath}/info`, (req, res) => {
    mongoConnection
      .validate(
        { _id: req.headers.customer },
        Joi.object().keys({
          _id: schemas._idField.required().label('customer_id')
        }),
        res
      )
      .then(validCustomerID => {
        mongoConnection.send(`/${validCustomerID._id}`, 'GET', {}, res);
      });
  });

  /**
   * consulta la información de clientes en la colección customers
   */
  app.get(`${apiPath}/`, (req, res) => {
    mongoConnection
      .validate(
        req.query,
        Joi.alternatives().try(
          Joi.object().keys({
            customer: schemas._customerID.required().label('customer_id')
          }),
          schemas.paginationSchema
        ),
        res
      )
      .then(validCustomerData => {
        mongoConnection.send(
          '',
          'GET',
          validCustomerData.customerID
            ? {
                q: JSON.stringify(validCustomerData)
              }
            : validCustomerData,
          res
        );
      });
  });

  /**
   * Importa el módulo de cuentas del cliente
   */
  require('./accounts/customerAccounts')(app);
};
