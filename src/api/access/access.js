const apiPath = '/api/access',
  mongoConnection = require('../../util/mongoConnection')('access'),
  Joi = require('joi'),
  schemas = require('../schemas'),
  crypto = require('crypto');

module.exports = app => {
  /**
   * Crea un nuevo registro de acceso de usuario en la colección access
   */
  app.post(`${apiPath}/`, (req, res) => {
    mongoConnection
      .validate(req.body, schemas.accessSchema, res)
      .then(validAccessData => {
        validAccessData.creationDate = { $date: new Date().toISOString() };
        return mongoConnection.send('', 'POST', validAccessData);
      })
      .then(access => {
        res
          .header('Content-Type', 'application/json')
          .status(200)
          .send(access)
          .end();
      });
  });

  /**
   * Consulta la información relacionada a un acceso de un usuario en la colección access
   */
  app.get(`${apiPath}/:_idAccess`, (req, res) => {
    mongoConnection
      .validate(
        req.params,
        Joi.object().keys({
          _idAccess: schemas._idField.required().label('access_id')
        }),
        res
      )
      .then(validAccessID => {
        return mongoConnection.send(`/${validAccessID._idAccess}/`, 'GET');
      })
      .then(access => {
        res
          .header('Content-Type', 'application/json')
          .status(200)
          .send(access)
          .end();
      })
      .catch(err => {
        res
          .header('Content-Type', 'application/json')
          .status(err.statusCode)
          .send(err)
          .end();
      });
  });

  /**
   * Consulta la información relacionada a un acceso de un usuario en la colección access
   */
  app.delete(`${apiPath}/:_idAccess`, (req, res) => {
    mongoConnection
      .validate(
        req.params,
        Joi.object().keys({
          _idAccess: schemas._idField.required().label('access_id')
        }),
        res
      )
      .then(validAccessID => {
        return mongoConnection.send(`/${validAccessID._idAccess}/`, 'DELETE', {}, res);
      });
  });
};
