const apiPath = '/api/accounts',
  mongoConnection = require('../../util/mongoConnection')('accounts'),
  Joi = require('joi'),
  schemas = require('../schemas');

module.exports = app => {
  /**
   * Crea una nueva cuenta en la colección accounts
   */
  app.post(`${apiPath}/`, (req, res) => {
    mongoConnection.validate(req.body, schemas.accountSchema, res).then(validAccountData => {
      mongoConnection.send('', 'POST', validAccountData, res);
    });
  });

  /**
   * Actualiza la información básica de una cuenta en la colección accounts
   */
  app.put(`${apiPath}/:_idAccount`, (req, res) => {
    let validAccountData;
    mongoConnection
      .validate(req.body, schemas.accountUpdateSchema, res)
      .then(_validAccountData => {
        validAccountData = _validAccountData;
        return mongoConnection.validate(
          req.params,
          Joi.object().keys({
            _idAccount: schemas._idField.required()
          }),
          res
        );
      })
      .then(validAccountID => {
        return mongoConnection.send(`/${validAccountID._idAccount}/`, 'PUT', { $set: validAccountData }, res);
      });
  });

  /**
   * Consulta la información básica de una cuenta en la colección accounts
   */
  app.get(`${apiPath}`, (req, res) => {
    mongoConnection
      .validate(
        req.query,
        Joi.alternatives().try(
          Joi.object().keys({
            accountID: schemas._accountID.required()
          }),
          schemas.paginationSchema
        ),
        res
      )
      .then(validAccountData => {
        return mongoConnection.send(
          '',
          'GET',
          validAccountData.accountID
            ? {
                q: JSON.stringify(validAccountData),
                f: JSON.stringify({ limit: 0, balance: 0 }),
                fo: true
              }
            : Object.assign(validAccountData, { f: JSON.stringify({ limit: 0, balance: 0 }) }),
          res
        );
      });
  });
};
