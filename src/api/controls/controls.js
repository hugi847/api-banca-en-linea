const apiPath = '/api/controls',
  mongoConnection = require('../../util/mongoConnection')('controls'),
  Joi = require('joi'),
  schemas = require('../schemas');

module.exports = app => {
  /**
   * Crea un nuevo registro de control de acceso de usuario en la colección controls
   */
  app.post(`${apiPath}/`, (req, res) => {
    mongoConnection
      .validate(req.body, schemas.accessControlSchema, res)
      .then(validAccessControlData => {
        return mongoConnection.send('', 'POST', validAccessControlData);
      })
      .then(accessControlData => {
        res
          .header('Content-Type', 'application/json')
          .status(200)
          .send(accessControlData)
          .end();
      });
  });

  /**
   * Consulta la información relacionada a un acceso de un usuario en la colección access
   */
  app.get(`${apiPath}/`, (req, res) => {
    mongoConnection
      .validate(
        { channel: req.query.channel },
        Joi.object().keys({
          channel: Joi.string()
            .min(4)
            .max(4)
            .required()
        }),
        res
      )
      .then(validChannel => {
        const q = { channel: validChannel.channel, $and: [] };
        if (req.query.user) {
          q.$and.push({
            $or: [
              {
                user: req.query.user
              },
              {
                user: {
                  $exists: false
                }
              }
            ]
          });
        }

        if (req.query.methods) {
          q.$and.push({
            $or: [
              {
                methods: req.query.methods
              },
              {
                methods: '*'
              }
            ]
          });
        }

        if (!q.$and.length) {
          delete q.$and;
        }

        const queryData = {
          q: JSON.stringify(q),
          s: JSON.stringify({
            user: -1,
            uri: -1
          })
        };
        return mongoConnection.send(``, 'GET', queryData, res);
      });
  });
};
